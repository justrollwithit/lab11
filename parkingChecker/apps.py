from django.apps import AppConfig


class ParkingcheckerConfig(AppConfig):
    name = 'parkingChecker'
