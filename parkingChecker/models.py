from django.db import models


# Create your models here.
class Entry(models.Model):
    location = models.CharField(max_length=20)
    datetime = models.DateTimeField(null=True)
