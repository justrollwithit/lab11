from django.views import View
from django.shortcuts import render
from .models import Entry


class MyView(View):
    def get(self, request):
        query_set_entry = Entry.objects.all()
        list_entry = list(query_set_entry)
        return render(request, "getMain.html", {"list": list_entry})

    def post(self, request):
        new_location = request.POST.get("location",None)
        new_datetime = request.POST.get("datetime", None)
        if new_location:
            new_entry = Entry(location=new_location, datetime=new_datetime)
            new_entry.save()
        query_set_entry = Entry.objects.all()
        list_entry = list(query_set_entry)
        return render(request, "getMain.html", {"list": list_entry})
